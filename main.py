import pyaudio
import sys
import wave
import matplotlib.pyplot as plt
import numpy as np
from array import array

chunk = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
#RATE = 8192
#RATE = 4096
RECORD_SECONDS = 5

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS, 
                rate=RATE, 
                input=True,
                output=True,
                frames_per_buffer=chunk)

frames = []

print "* recording"
for i in range(0, RATE / chunk * RECORD_SECONDS):
    data = stream.read(chunk)
    data_chunk=array('h', data)
    print "len(data_chunk)=", len(data_chunk)
    print "data_chunk[0]=", data_chunk[0:16]
    vol = max(data_chunk)
    if (vol >= 6000):
        print ("vol=", vol)
    else:
        print ("silent=", vol)

    frames.append(data)


print "* done"

sig = np.frombuffer(data, dtype='<i2').reshape(-1, CHANNELS)
#normalized = utility.pcm2float(sig, np.float32)
plt.plot(sig)
plt.show()

stream.stop_stream()
stream.close()
p.terminate()

wf = wave.open('recorded.wav', 'wb')
wf.setnchannels(CHANNELS)
wf.setsampwidth(p.get_sample_size(FORMAT))
wf.setframerate(RATE)
wf.writeframes(b''.join(frames))
wf.close()
